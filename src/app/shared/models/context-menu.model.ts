export interface ContextMenuModel {
  text: string;
  eventName: string;
}
