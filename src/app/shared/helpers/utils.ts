import {ERROR_MESSAGES} from "./constants";

export function formatDate(dateToFormat: Date) {
  return `${dateToFormat.getFullYear()}-${(dateToFormat.getMonth() + 1).toString().padStart(2, '0')}-${dateToFormat.getDate().toString().padStart(2, '0')}`
}

export function parseError(err: any){
  let errorMessage = ERROR_MESSAGES.DEFAULT;
  console.error(err);
  if(err && err.error && err.error.status == 206){
    errorMessage = ERROR_MESSAGES["206"];
  }
  return errorMessage;
}
