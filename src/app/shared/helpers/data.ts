import {FinancialProduct} from "../../features/financial-products-flow/models/financial-product";

export const DATA: FinancialProduct[] = [
  {
    id: 'trj-1',
    name: 'Tarjetas de crédito',
    description: 'Tarjeta de consumo bajo la modalidad de crédito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-2',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-3',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-4',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-5',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-6',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-7',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-8',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-9',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
  {
    id: 'trj-10',
    name: 'Tarjetas de debito',
    description: 'Tarjeta de consumo bajo la modalidad de debito',
    logo: '',
    date_release: new Date(),
    date_revision: new Date()
  },
]
