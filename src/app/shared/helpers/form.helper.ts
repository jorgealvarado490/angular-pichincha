import {FormGroup} from "@angular/forms";

export function hasError(fomGroup: FormGroup, controlName: string, errorsNames: string[]){
  const result: boolean[] = new Array(errorsNames.length).fill(false);
  const control = fomGroup.get(controlName);
  if(control){
    errorsNames.forEach((it, idx) => {
      if(control.hasError(it)){
        result[idx] = true;
      }
    })
  }

  return result.reduce((acc, curr) => { return acc || curr});
}
