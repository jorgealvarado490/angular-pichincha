export const ERROR_MESSAGES = {
  DEFAULT: 'Ocurrió un error procesando la petición',
  206: 'Revisar, alguno de los campos no han sido ingresados correctamente',
  401: 'No es posible realizar ésta modificación debido a que este producto no te pertenece',
  404: 'No se encontró el producto con el id proporcionado'
}
