import {inject, TestBed} from '@angular/core/testing';

import {HeadersInterceptor} from './headers.interceptor';
import {HttpEventType, HttpRequest, HttpSentEvent} from "@angular/common/http";
import {of} from "rxjs";


describe('HeadersInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HeadersInterceptor
    ]
  }));

  it('should be created', () => {
    const interceptor: HeadersInterceptor = TestBed.inject(HeadersInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should intercept successfully', inject([HeadersInterceptor],
    (interceptor: HeadersInterceptor) => {
      const outputCheck = {type: HttpEventType.Sent} as HttpSentEvent;
      const request = new HttpRequest('GET', 'mockUrl');
      const handler = {
        handle: (req: HttpRequest<any>) => {
          expect(req.url).toEqual('mockUrl');
          expect(req.headers.get('authorId')).toEqual('00200627');
          return of(outputCheck);
        }
      };
      interceptor.intercept(request, handler)
        .subscribe(data => {
          expect(data).toEqual(outputCheck);
        });

    }))
});
