import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ContextMenuComponent} from './context-menu.component';

describe('ContextMenuComponent', () => {
  let component: ContextMenuComponent;
  let fixture: ComponentFixture<ContextMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContextMenuComponent]
    });
    fixture = TestBed.createComponent(ContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Output Tests', () => {
    it('should emit the correct item when its clicked', () => {
      const spyOuput = jest.spyOn(component.onContextMenuItemClick, 'emit');
      fixture.detectChanges();
      component.onContextMenuClick({}, {eventName: 'delete', text: 'Delete'})
      expect(spyOuput).toHaveBeenCalled()
    })
  })
});
