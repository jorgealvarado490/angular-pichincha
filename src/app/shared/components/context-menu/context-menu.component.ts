import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {ContextMenuModel} from "../../models/context-menu.model";

@Component({
  selector: 'pch-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContextMenuComponent {
  @Input({required: true
  })
  contextMenuItems!: Array<ContextMenuModel>;

  @Output()
  onContextMenuItemClick: EventEmitter<any> = new EventEmitter<any>();

  onContextMenuClick(event: any, data: any) {
    this.onContextMenuItemClick.emit({
      event,
      data,
    });
  }
}
