import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'pch-menu-icon',
  templateUrl: './menu-icon.component.html',
  styleUrls: ['./menu-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuIconComponent {

  @Output() click = new EventEmitter();

  onClick(e: any) {
    this.click.emit({
      clientX: e.clientX,
      clientY: e.clientY
    });
  }

}
