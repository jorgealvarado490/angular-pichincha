import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'pch-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input() variant: 'default' | 'primary' | 'danger' = 'default';

  @Input() disabled = false;

  @Output() onClick = new EventEmitter();

  onButtonClick(){
    this.onClick.emit();
  }
}
