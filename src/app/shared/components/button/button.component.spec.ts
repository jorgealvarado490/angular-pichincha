import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonComponent } from './button.component';
import {By} from "@angular/platform-browser";

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent]
    });
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('Inputs Tests', () => {
    it('should set the correct classes when its primary variant', ()=> {
      component.variant = 'primary';
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('[test-id="button"]')).nativeElement;
      expect(element.classList.contains('button')).toBeTruthy();
      expect(element.classList.contains('button-primary')).toBeTruthy();
    })

    it('should set the correct classes when its danger variant', ()=> {
      component.variant = 'danger';
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('[test-id="button"]')).nativeElement;
      expect(element.classList.contains('button')).toBeTruthy();
      expect(element.classList.contains('button-danger')).toBeTruthy();
    })

    it('should set the disabled state on button when the disabled property is passed', () => {
      // Arrange
      component.disabled = true;
      // Act
      fixture.detectChanges();
      // Assert
      const element = fixture.debugElement.query(By.css('[test-id="button"]')).nativeElement;
      expect(element.getAttribute('disabled')).toEqual("");
    })

    it('should remove the disabled state on button when the disabled property is passed', () => {
      // Arrange
      component.disabled = false;
      // Act
      fixture.detectChanges();
      // Assert
      const element = fixture.debugElement.query(By.css('[test-id="button"]')).nativeElement;
      expect(element.getAttribute('disabled')).toEqual(null);
    })
  });

  describe('Outputs Tests', () => {
    it('should emit the value', () => {
      const spyOutput = jest.spyOn(component.onClick, 'emit');
      fixture.detectChanges();
      component.onButtonClick();
      expect(spyOutput).toHaveBeenCalled();
    })
  })
});
