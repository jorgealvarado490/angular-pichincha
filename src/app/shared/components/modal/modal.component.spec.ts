import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalComponent } from './modal.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ModalComponent]
    });
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;

  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('Component Style', () => {
    it('should change the display style when the modal was opened', () => {
      fixture.detectChanges();
      component.open();
      expect(component.modal.nativeElement.style.display).toEqual('block');
    })

    it('should change the display style when cancel button is clicked', () => {
      fixture.detectChanges();
      component.handleClose();
      expect(component.modal.nativeElement.style.display).toEqual('none');
    })
  })

  describe('Output Tests', () => {
    it('should emit the correct item when its clicked', () => {
      const spyOutput = jest.spyOn(component.onConfirm, 'emit');
      fixture.detectChanges();
      component.handleConfirm()
      expect(spyOutput).toHaveBeenCalled()
    })
  })
});
