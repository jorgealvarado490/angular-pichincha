import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'pch-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent {
  @ViewChild('myModal', {static: false}) modal!: ElementRef;

  @Output() onConfirm = new EventEmitter();

  open() {
    this.modal.nativeElement.style.display = 'block';
  }

  handleClose(){
    this.modal.nativeElement.style.display = 'none';
  }

  handleConfirm() {
    this.onConfirm.emit(true);
  }
}
