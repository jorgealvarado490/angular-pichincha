import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'pch-info-icon',
  templateUrl: './info-icon.component.html',
  styleUrls: ['./info-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoIconComponent {

}
