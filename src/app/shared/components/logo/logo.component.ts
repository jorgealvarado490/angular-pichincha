import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'pch-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoComponent {

  defaultRouteLogo = '/assets/images/visa.png'
  @Input() src = 'assets/images/visa.png';

}
