import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import {trigger} from "@angular/animations";
import {of, Subscription} from "rxjs";

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent]
    });
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;

  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('Output Tests', () => {
    it('should set a value for the subject when input method is called', () => {
      const spyOutput = jest.spyOn(component.inputValue, 'next');
      fixture.detectChanges();
      component.onInput({target: {value: 'Test'}})
      expect(spyOutput).toHaveBeenCalled()
    })

    it('should call the subscribe method on ngOnInit hook', () => {
      const spy = jest.spyOn(component.trigger, 'subscribe');
      fixture.detectChanges();
      expect(spy).toHaveBeenCalled();
    });

    it('should call the emit value when the subscribe method is called', () => {
      jest.spyOn(component.inputValue, 'next').mockReturnValue();
      jest.spyOn(component.trigger, 'subscribe').mockImplementation((value) => new Subscription());
      fixture.detectChanges();
      component.trigger.subscribe((value) => {
        const spy = jest.spyOn(component.textChange, 'emit');
        expect(spy).toHaveBeenCalled();
      })
    });
  })
});
