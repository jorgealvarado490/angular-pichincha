import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { SearchComponent } from './components/search/search.component';
import {ReactiveFormsModule} from "@angular/forms";
import { InfoIconComponent } from './components/info-icon/info-icon.component';
import { MenuIconComponent } from './components/menu-icon/menu-icon.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { LogoComponent } from './components/logo/logo.component';
import { ModalComponent } from './components/modal/modal.component';
import { WithLoadingPipe } from './pipes/with-loading.pipe';



@NgModule({
  declarations: [
    ButtonComponent,
    SearchComponent,
    InfoIconComponent,
    MenuIconComponent,
    ContextMenuComponent,
    LogoComponent,
    ModalComponent,
    WithLoadingPipe
  ],
  imports: [
    CommonModule,
    NgOptimizedImage
  ],
  exports: [
    ButtonComponent,
    SearchComponent,
    InfoIconComponent,
    MenuIconComponent,
    ContextMenuComponent,
    LogoComponent,
    ModalComponent,
    WithLoadingPipe
  ]
})
export class SharedModule { }
