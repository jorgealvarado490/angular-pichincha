import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataShareService<T> {

  private data$ = new BehaviorSubject<T | null>(null);
  constructor() { }

  set data(dataToChange: T | null){
    this.data$.next(dataToChange);
  }

  get data(): T | null{
    return this.data$.getValue();
  }
}
