import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoreComponent} from "./core.component";

const routes: Routes = [
  {
    path: 'financial-products',
    component: CoreComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../features/financial-products-flow/financial-products.module').then(m => m.FinancialProductsModule)
      }
    ],
  },
  {
    path: '',
    redirectTo: 'financial-products',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
