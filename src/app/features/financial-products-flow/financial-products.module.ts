import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancialProductsRoutingModule } from './financial-products-routing.module';
import { ListComponent } from './pages/list/list.component';
import {SharedModule} from "../../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CreateComponent } from './pages/create/create.component';
import {FinancialProductsService} from "./services/financial-products.service";
import {HttpClientModule} from "@angular/common/http";
import { EditComponent } from './pages/edit/edit.component';


@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    EditComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        FinancialProductsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
  providers: [
    FinancialProductsService,
  ]
})
export class FinancialProductsModule { }
