import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateComponent} from './create.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {FinancialProductsService} from "../../services/financial-products.service";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {By} from "@angular/platform-browser";
import {IdValidator} from "../../validators/IdValidator";
import {lastValueFrom, of} from "rxjs";
import {Router} from "@angular/router";

describe('CreateComponent', () => {
  let component: CreateComponent;
  let fixture: ComponentFixture<CreateComponent>;
  let router: Router;

  const financialProductService = {
    verifyId: jest.fn(),
    removeProduct: jest.fn(),
    createProduct: jest.fn(),
    editProduct: jest.fn(),
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [CreateComponent],
      providers: [{provide: FinancialProductsService, useValue: financialProductService}],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
      ]
    });
    fixture = TestBed.createComponent(CreateComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should set the revision date correctly on the form when the release date changed', () => {
    component.frmCreate.get('date_release')?.setValue(['2023-11-16']);
    fixture.detectChanges();
    component.handleReleaseDateChange();
    expect(component.frmCreate.get('date_revision')?.value).toEqual('2024-11-16')
  })


  it('should set disabled to false when all inputs are valid', () => {
    financialProductService.verifyId.mockImplementation(() => of(false));
    jest.spyOn(IdValidator, 'createValidator');
    component.frmCreate.patchValue({
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: ["2023-11-17"],
      date_revision: "2024-11-17"
    })
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css("[test-id='btn-send']")).nativeElement;
    expect(element.disabled).toBeTruthy();
  });


  it('should call the service to create a new product when all inputs are valid', async () => {
    financialProductService.verifyId.mockImplementation(() => of(false));
    jest.spyOn(IdValidator, 'createValidator');
    const routerNavigateSpy = jest
      .spyOn(router, 'navigate')
      .mockImplementation(() => lastValueFrom(of(true)));
    financialProductService.createProduct.mockImplementation(() => of({
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: ["2023-11-17"],
      date_revision: "2024-11-17"
    }));

    component.frmCreate.patchValue({
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: ["2023-11-17"],
      date_revision: "2024-11-17"
    })

    fixture.detectChanges();
    await component.handleSave();

    expect(financialProductService.createProduct).toHaveBeenCalled();
    expect(routerNavigateSpy).toHaveBeenCalled();
  });

  it('should set the message error when some error happens', async () => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
    jest.spyOn(IdValidator, 'createValidator');
    financialProductService.verifyId.mockImplementation(() => of(false));
    financialProductService.createProduct.mockImplementation(() => {
      throw new Error('Error')
    });
    component.frmCreate.patchValue({
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: ["2023-11-17"],
      date_revision: "2024-11-17"
    })
    fixture.detectChanges();
    await component.handleSave();

    expect(component.errorMessage).toBeTruthy();
  });

  it('should reset the form when call method reboot', () => {
    const spyReset = jest.spyOn(component.frmCreate, 'reset');
    component.handleReboot();
    expect(spyReset).toHaveBeenCalled();
  })
});
