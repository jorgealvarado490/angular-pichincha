import {Component, inject} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FinancialProductsService} from "../../services/financial-products.service";
import {IdValidator} from "../../validators/IdValidator";
import {hasError} from "../../../../shared/helpers/form.helper";
import {dateReleaseValidator} from "../../validators/DateValidator";
import {lastValueFrom} from "rxjs";
import {FinancialProduct} from "../../models/financial-product";
import {Router} from "@angular/router";
import {formatDate, parseError} from "../../../../shared/helpers/utils";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  private readonly fb: FormBuilder = inject(FormBuilder);
  private readonly router = inject(Router);
  private readonly financialProductsService = inject(FinancialProductsService);
  protected readonly hasError = hasError;
  isPending = false;
  isShowingError = false;
  errorMessage = '';

  frmCreate = this.fb.nonNullable.group({
    id: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10)], [IdValidator.createValidator(this.financialProductsService)]],
    name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    logo: ['', [Validators.required, Validators.pattern(/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/)]],
    description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
    date_release: ['', [Validators.required, dateReleaseValidator]],
    date_revision: [{value: '', disabled: true}, Validators.required],
  })

  handleReleaseDateChange() {
    const dateReleaseRaw = this.frmCreate.get('date_release')?.value as unknown as string;
    const dateRelease = new Date(Date.parse(dateReleaseRaw));
    const dateRevision = new Date(dateRelease.getFullYear() + 1, dateRelease.getMonth(), dateRelease.getDate() + 1);
    this.frmCreate.patchValue({
      date_revision: formatDate(dateRevision)
    })
  }

  async handleSave() {
    try {
      this.isPending = true;
      const financialProduct: FinancialProduct = this.frmCreate.getRawValue() as unknown as FinancialProduct;
      await lastValueFrom(this.financialProductsService.createProduct(financialProduct))
      await this.router.navigate(['/financial-products'], {replaceUrl: true});
    } catch (err: any) {
      this.errorMessage = parseError(err);
    } finally {
      this.isPending = false;
    }
  }

  handleReboot() {
    this.frmCreate.reset();
    this.frmCreate.markAsUntouched();
  }

}
