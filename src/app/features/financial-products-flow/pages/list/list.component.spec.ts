import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ListComponent} from './list.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {Router} from "@angular/router";
import {FinancialProductsService} from "../../services/financial-products.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {SharedModule} from "../../../../shared/shared.module";
import {lastValueFrom, of} from "rxjs";
import {DATA} from "../../../../shared/helpers/data";
import {By} from "@angular/platform-browser";
import spyOn = jest.spyOn;
import {EditComponent} from "../edit/edit.component";

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let router: Router;

  const financialProductService = {
    verifyId: jest.fn(),
    removeProduct: jest.fn(),
    createProduct: jest.fn(),
    editProduct: jest.fn(),
    getAll: jest.fn()
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: FinancialProductsService, useValue: financialProductService}],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        FormsModule,
        SharedModule
      ],
      declarations: [ListComponent],
    });
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should filter correctly the observable root property', () => {
    // Arrange
    component.products$ = of(DATA);
    // Act
    component.getProductsFiltered('trj', 5);
    // Assert
    component.filteredProducts$.subscribe(it => {
      expect(it.length).toBe(5);
    })
  })

  it('should set the text to the subject', () => {
    const spySearch = spyOn(component.searchTerm$, 'next');

    component.onSearchChange('term');

    expect(spySearch).toHaveBeenCalled();
  })

  it('should redirect when click button Agregar', () => {
    const spyRouter = jest.spyOn(router, 'navigate');
    const element = fixture.debugElement.query(By.css("[test-id='btn-add']")).nativeElement;
    element.click();
    fixture.detectChanges();
    expect(spyRouter).toHaveBeenCalled();
  })

  it('should open the modal when an item with delete is passed to the method', () => {
    fixture.detectChanges()
    const spyModal = jest.spyOn(component.modal, 'open');
    const selectedItem = {
      data: 'delete'
    };
    component.handleMenuItemClicked(selectedItem);
    expect(spyModal).toHaveBeenCalled();
  })

  it('should navigate to the edit page when an item is passed to the method', () => {
    component.currentProduct = {
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: new Date(),
      date_revision: new Date()
    }
    const spyRouter = jest.spyOn(router, 'navigate').mockImplementation(() => lastValueFrom(of(true)));
    const selectedItem = {
      data: 'edit'
    };
    fixture.detectChanges();
    component.handleMenuItemClicked(selectedItem);
    expect(spyRouter).toHaveBeenCalled();
  })
});
