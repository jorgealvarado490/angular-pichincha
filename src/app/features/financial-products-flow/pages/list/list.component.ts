import {Component, inject, ViewChild} from '@angular/core';
import {BehaviorSubject, combineLatest, lastValueFrom, Observable, of, switchMap} from "rxjs";
import {FinancialProduct} from "../../models/financial-product";
import {ContextMenuModel} from "../../../../shared/models/context-menu.model";
import {Router} from "@angular/router";
import {ModalComponent} from "../../../../shared/components/modal/modal.component";
import {FinancialProductsService} from "../../services/financial-products.service";
import {DataShareService} from "../../../../shared/services/data-share.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {
  private readonly router = inject(Router);
  private readonly financialProductService = inject(FinancialProductsService);
  private readonly dataShareService = inject(DataShareService<FinancialProduct>);

  products$: Observable<FinancialProduct[]> = this.financialProductService.getAll();
  searchTerm$ = new BehaviorSubject<string>('');
  rowsPerPage$ = new BehaviorSubject(5);
  filteredProducts$: Observable<FinancialProduct[]> = combineLatest([this.searchTerm$, this.rowsPerPage$]).pipe(
    switchMap(([term, rowsPerPage]) => this.getProductsFiltered(term, rowsPerPage))
  )

  @ViewChild('modal', {static: false}) modal!: ModalComponent
  isShowedContextMenu = false;
  currentProduct: FinancialProduct | null = null;
  menuItems: ContextMenuModel[] = [
    {
      text: 'Editar',
      eventName: 'edit'
    },
    {
      text: 'Eliminar',
      eventName: 'delete'
    }
  ]
  panelStyle = {};

  isShowingErrorMessage = false;
  errorMessage = 'Ocurrió un error al procesar la petición';

  getProductsFiltered(term: string, rowsPerPage: number) {
    if (!term) return this.products$.pipe(switchMap(products => {
      return of(products.slice(0, rowsPerPage))
    }));
    return this.products$.pipe(
      switchMap((value) => {
        return of(value
          .filter(product =>
            product.name.toLocaleLowerCase().includes(term.toLocaleLowerCase())
            || product.description.toLocaleLowerCase().includes(term.toLocaleLowerCase()))
          .slice(0, rowsPerPage))
      })
    )
  }

  onSearchChange(termSearch: string) {
    this.searchTerm$.next(termSearch);
  }

  onSelectChange(e: any) {
    this.rowsPerPage$.next(e.target.value);
  }

  onContextMenuClick(event: any, product: FinancialProduct) {
    this.panelStyle = {
      'position': 'absolute',
      'left.px': event.clientX,
      'top.px': event.clientY
    }
    this.currentProduct = product;
    this.isShowedContextMenu = true;
  }

  handleAddProduct() {
    this.router.navigate(['/financial-products/create']).then(console.log);
  }

  handleMenuItemClicked(e: any) {
    if (e.data === 'delete') {
      this.modal.open();
    }
    if (e.data === 'edit') {
      this.dataShareService.data = this.currentProduct;
      this.router.navigate(['/financial-products/edit/' + this.currentProduct?.id!]);
    }
  }

  async handleConfirm(e: boolean) {
    try {
      this.isShowingErrorMessage = false;
      if (e && this.currentProduct) {
        await lastValueFrom(this.financialProductService.removeProduct(this.currentProduct.id!));
        this.modal.handleClose();
        this.rowsPerPage$.next(5);
        this.currentProduct = null;
      }
    } catch (err: any) {
      this.isShowingErrorMessage = true;
      console.error(err);
      if (err && err.error && err.error.status == 404) {
        this.errorMessage = 'No se encontró el producto con el id proporcionado';
      }
    }
  }
}
