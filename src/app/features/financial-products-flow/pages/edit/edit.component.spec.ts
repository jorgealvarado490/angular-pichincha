import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditComponent} from './edit.component';
import {Router} from "@angular/router";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {FinancialProductsService} from "../../services/financial-products.service";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {DataShareService} from "../../../../shared/services/data-share.service";
import {of} from "rxjs";

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;
  let router: Router;

  const financialProductService = {
    verifyId: jest.fn(),
    removeProduct: jest.fn(),
    createProduct: jest.fn(),
    editProduct: jest.fn(),
  }

  const dataShareService = {
    data: jest.fn()
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: FinancialProductsService, useValue: financialProductService}, {
        provide: DataShareService,
        useValue: dataShareService
      }],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
      ],
      declarations: [EditComponent]
    });
    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    component.productId = 'trj-555';
    dataShareService.data.mockImplementation(() => of({
      id: "trj-555",
      name: "Tarjeta de Credito André",
      description: "Tarejta de Credito para André",
      logo: "https://w7.pngwing.com/pngs/49/82/png-transparent-credit-card-visa-logo-mastercard-bank-mastercard-blue-text-rectangle.png",
      date_release: ["2023-11-17"],
      date_revision: "2024-11-17"
    }));
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
