import {Component, inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {FinancialProductsService} from "../../services/financial-products.service";
import {dateReleaseValidator} from "../../validators/DateValidator";
import {FinancialProduct} from "../../models/financial-product";
import {hasError} from '../../../../shared/helpers/form.helper';
import {lastValueFrom} from "rxjs";
import {DataShareService} from "../../../../shared/services/data-share.service";
import {formatDate, parseError} from "../../../../shared/helpers/utils";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  private readonly fb: FormBuilder = inject(FormBuilder);
  private readonly router = inject(Router);
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly financialProductsService = inject(FinancialProductsService);
  private readonly dataShareService = inject(DataShareService<FinancialProduct>);
  protected readonly hasError = hasError;

  productId = '';
  isPending = false;
  errorMessage = '';

  frmEdit = this.fb.nonNullable.group({
    id: [{value: '', disabled: true}],
    name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    logo: ['', [Validators.required, Validators.pattern(/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/)]],
    description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
    date_release: ['', [Validators.required, dateReleaseValidator]],
    date_revision: [{value: '', disabled: true}, Validators.required],
  })

  ngOnInit() {
    this.productId = this.activatedRoute.snapshot.params['id'];
    if (!this.productId) {
      this.router.navigate(['/financial-products'], {replaceUrl: true}).then(console.log);
    }
    const selectedProduct = this.dataShareService.data;
    if (!selectedProduct) {
      this.router.navigate(['/financial-products'], {replaceUrl: true}).then(console.log);
      return;
    }

    selectedProduct.date_release = new Date(selectedProduct.date_release);
    selectedProduct.date_revision = new Date(selectedProduct.date_revision);
    this.frmEdit.patchValue({
      id: this.productId,
      name: selectedProduct.name,
      logo: selectedProduct.logo,
      description: selectedProduct.description,
      date_release: [formatDate(selectedProduct.date_release)],
      date_revision: formatDate(selectedProduct.date_revision)
    })
  }

  handleReleaseDateChange() {
    const dateReleaseRaw = this.frmEdit.get('date_release')?.value as unknown as string;
    const dateRelease = new Date(Date.parse(dateReleaseRaw));
    const dateRevision = new Date(dateRelease.getFullYear() + 1, dateRelease.getMonth(), dateRelease.getDate() + 1);
    this.frmEdit.patchValue({
      date_revision: formatDate(dateRevision)
    })
  }

  async handleSave() {
    try {
      const financialProduct: FinancialProduct = this.frmEdit.getRawValue() as unknown as FinancialProduct;
      financialProduct.date_release = new Date((financialProduct.date_release as unknown as string[])[0])
      await lastValueFrom(this.financialProductsService.editProduct(financialProduct))
      await this.router.navigate(['/financial-products'], {replaceUrl: true});
    } catch (err: any) {
      this.errorMessage = parseError(err);
    }
  }

}
