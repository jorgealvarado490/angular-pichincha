import {inject, TestBed} from '@angular/core/testing';

import { FinancialProductsService } from './financial-products.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {API_BASE_URL} from "../../../app.module";
import {FinancialProduct} from "../models/financial-product";
import {HttpRequest} from "@angular/common/http";

describe('FinancialProductsService', () => {
  let service: FinancialProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [FinancialProductsService, {
        provide: API_BASE_URL,
        useValue: 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products'
      }]
    });
    service = TestBed.inject(FinancialProductsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should create a new product', inject([FinancialProductsService, HttpTestingController],
    (financialService: FinancialProductsService, backend: HttpTestingController) => {

      const mockResponse: FinancialProduct[] = [{
        id: 'trj-555',
        name: 'Tarjeta de prueba',
        description: 'Tarjeta de prueba',
        logo: 'logo',
        date_revision: new Date(),
        date_release: new Date(),
      }];


      const formData: FinancialProduct = {
        id: 'trj-555',
        name: 'Tarjeta de prueba',
        description: 'Tarjeta de prueba',
        logo: 'logo',
        date_revision: new Date(),
        date_release: new Date(),
      };

      financialService.createProduct(formData).subscribe(data => {
        expect(data).toEqual(mockResponse);
      });

      backend.expectOne((request: HttpRequest<any>) => {
        return request.method === 'POST'
          && request.url === 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products'
          && JSON.stringify(request.body) === JSON.stringify(formData);
      }).flush(mockResponse);
    }));


  it('should update a product', inject([FinancialProductsService, HttpTestingController],
    (financialService: FinancialProductsService, backend: HttpTestingController) => {

      const mockResponse: FinancialProduct[] = [{
        id: 'trj-555',
        name: 'Tarjeta de prueba',
        description: 'Tarjeta de prueba',
        logo: 'logo',
        date_revision: new Date(),
        date_release: new Date(),
      }];


      const formData: FinancialProduct = {
        id: 'trj-555',
        name: 'Tarjeta de prueba',
        description: 'Tarjeta de prueba',
        logo: 'logo',
        date_revision: new Date(),
        date_release: new Date(),
      };

      financialService.editProduct(formData).subscribe(data => {
        expect(data).toEqual(mockResponse);
      });

      backend.expectOne((request: HttpRequest<any>) => {
        return request.method === 'PUT'
          && request.url === 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products'
          && JSON.stringify(request.body) === JSON.stringify(formData);
      }).flush(mockResponse);
    }));

  it('should get all products', inject([FinancialProductsService, HttpTestingController],
    (financialService: FinancialProductsService, backend: HttpTestingController) => {

      const mockResponse: FinancialProduct[] = [{
        id: 'trj-555',
        name: 'Tarjeta de prueba',
        description: 'Tarjeta de prueba',
        logo: 'logo',
        date_revision: new Date(),
        date_release: new Date(),
      }];


      financialService.getAll().subscribe(data => {
        expect(data).toEqual(mockResponse);
      });

      backend.expectOne((request: HttpRequest<any>) => {
        return request.method === 'GET'
          && request.url === 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products'
      }).flush(mockResponse);
    }));

  it('should verify if the id exists or no', inject([FinancialProductsService, HttpTestingController],
    (financialService: FinancialProductsService, backend: HttpTestingController) => {

      const mockResponse= false;


      financialService.verifyId('trj-555').subscribe(data => {
        expect(data).toEqual(mockResponse);
      });

      backend.expectOne((request: HttpRequest<any>) => {
        return request.method === 'GET'
          && request.url === 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products/verification?id=trj-555'
      }).flush(mockResponse);
    }));

  it('should remove the given product by id', inject([FinancialProductsService, HttpTestingController],
    (financialService: FinancialProductsService, backend: HttpTestingController) => {

      const mockResponse: string= 'Produce removed succesfully';


      financialService.removeProduct('trj-555').subscribe(data => {
        expect(data).toEqual(mockResponse);
      });

      backend.expectOne((request: HttpRequest<any>) => {
        return request.method === 'DELETE'
          && request.url === 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products?id=trj-555'
      }).flush(mockResponse);
    }));
});
