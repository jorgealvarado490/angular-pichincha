import {Inject, Injectable, InjectionToken} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API_BASE_URL} from "../../../app.module";
import {FinancialProduct} from "../models/financial-product";
import {filter, map, Observable, of, switchMap} from "rxjs";


@Injectable()
export class FinancialProductsService {

  constructor(private readonly http: HttpClient,@Inject(API_BASE_URL) private readonly  baseUrl?: string) { }


  verifyId(id: string){
    return this.http.get<boolean>(`${this.baseUrl}/verification?id=${id}`);
  }

  createProduct(product: FinancialProduct){
    return this.http.post(`${this.baseUrl}`, product);
  }

  editProduct(product: FinancialProduct){
    return this.http.put(`${this.baseUrl}`, product);
  }

  removeProduct(productId: string){
    // @ts-ignore
    return this.http.delete<string>(`${this.baseUrl}?id=${productId}`, {responseType: 'text'});
  }

  getAll():Observable<FinancialProduct[]> {
    return this.http.get<FinancialProduct[]>(`${this.baseUrl}`);
  }

}
