import {FinancialProductsService} from "../services/financial-products.service";
import {AbstractControl, AsyncValidatorFn, ValidationErrors} from "@angular/forms";
import {map, Observable} from "rxjs";

export class IdValidator {
  static createValidator(financialProductService: FinancialProductsService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return financialProductService
        .verifyId(control.value)
        .pipe(
          map((result: boolean) => {
              return result ? {idAlreadyExists: true} as ValidationErrors : null;
            }
          )
        );
    };
  }
}
