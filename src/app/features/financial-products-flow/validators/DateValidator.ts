import {FormControl} from "@angular/forms";

export function dateReleaseValidator(control: FormControl) {
  const rawCurrentDate = new Date();
  const rawReleaseDate = new Date(Date.parse(control.value));

  const currentDate = new Date(rawCurrentDate.getFullYear(), rawCurrentDate.getMonth(), rawCurrentDate.getDate());
  const releaseDate = new Date(rawReleaseDate.getFullYear(), rawReleaseDate.getMonth(), rawReleaseDate.getDate()+1);

  if (releaseDate.getDate() < currentDate.getDate()) {
    return {
      incorrectDate: true
    }
  }
  return null;
}
